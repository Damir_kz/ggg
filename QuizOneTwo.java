package com.company;

import java.util.Scanner;

public class QuizOneTwo {

    public static void main(String[] args) {

    }

    // Задача 1
    public static int convertToDec(int number, int k) {
        int sum=0;
        int step=1;
        int digit=0;
        while (number > 0) {
            digit=number % 10;
            sum=sum + digit * step;
            step=step * k;
            number=number / 10;
        }
        return sum;
    }

    // Задача 2
    public static long countCombinations(int n, int m) {
        int r1=n + m - 1;
        int r2=n - 1;
        long sum=1;
        for (int i=1; i < n + m; i++)
            sum=sum * i;
        for (int i=1; i < n; i++) {
            sum=sum / i;
        }
        for (int i=1; i < m + 1; i++) {
            sum=sum / i;
        }
        return sum;
    }
}
    //Задача 3
